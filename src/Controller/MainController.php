<?php
namespace App\Controller;

use App\Entity\Maleteo;
use App\Form\MaleteoFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController{


    /**
     * @Route("/maleteo", name="maleteo")
     */
    public function inicio(){
        return $this->render("maleteo.html.twig");

    }

    /**
     * @Route("/maleteo/nueva", name="nuevo registro")
     */
   
     public function nuevaMaleteo(){

        $form = $this->createForm(MaleteoFormType::class);
         return $this->render(
             "maleteo/nueva.html.twig",[
"formulario"=>$form->createView()
             ]
             );
     }

}